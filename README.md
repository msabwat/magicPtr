Link to webapp:
https://cocky-perlman-ace9c9.netlify.com/

#Install:
apt-get install libevdev-dev

make sure LIBEVDEV_P path is accurate
run MAKE
make clean : to delete executables

# Notes:

Run make to create executable

EXECUTE: (after sudo su)
. env/bin/activate (if you don't have python3 correctly setup with PIP) 
python3 server.py

Be careful with the IP address, if you have a local one you will need to connect 
to the webapp in the same network, or else hardcode the public IP address in 
server.py and in the webapp.

####mouseMove usage####

it reads from stdin two space separated integers representing the relative x and y movement