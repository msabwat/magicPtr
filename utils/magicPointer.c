#include "magicPointer.h"

int main(void)
{

  FILE *pipein, *pipeout;
  char buf[80];

  struct input_event keyPress;
  int kb = open("/dev/input/event3", O_RDONLY | O_NONBLOCK);
  int kbBytes = 0;

  if (kb == -1)
    {
      printf("Error opening keyboard file descriptor, are your root?\n");
      exit(1);
    }
  if (( pipein = popen(". env/bin/activate && python3 server.py", "r")) == NULL)
    {
      perror("popen");
      exit(1);
    }
  if (( pipeout = popen("./moveMouse", "w")) == NULL)
    {
      perror("popen");
      exit(1);
    }
  
  while (42)
    {
      kbBytes = read(kb, &keyPress, sizeof(keyPress)); 
      if (keyPress.value == 30)
	break;
      fgets(buf, 80, pipein); // get data from server.py stdout
      fputs(buf, pipeout); // send it to moveMouse stdin
    }
  close(kb);
  close((int) pipein);
  close((int) pipeout);
  return 0;
}
