moveMouse	=	moveMouse

CC		= 	gcc

LIBFT_P		=	./utils/libft

LIBEVDEV_P 	= 	/usr/local/include/libevdev-1.0/libevdev

all: build

build:$(libft)
	@$(MAKE) -C $(LIBFT_P)
	@$(MAKE) -C $(LIBFT_P) clean	
	@echo "Created libft.a"
	@$(CC) moveMouse.c -o $(moveMouse) -I$(LIBEVDEV_P) -L$(LIBFT_P) -lft -levdev
	@echo "Created moveMouse"

clean:
	@echo "Cleaning objects"
	@$(RM) $(moveMouse) a.out
	@echo "Cleaning libft"	
	@$(MAKE) -C $(LIBFT_P) fclean

re: clean all
