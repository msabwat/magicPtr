#include "magicPointer.h"

void moveMouse(struct libevdev_uinput *magicPointer, int x, int y)
{
  usleep(15000);//1 ms works but not always, putting 15ms to be safe
  libevdev_uinput_write_event(magicPointer, EV_REL, REL_X, x);
  libevdev_uinput_write_event(magicPointer, EV_REL, REL_Y, y);
  libevdev_uinput_write_event(magicPointer, EV_SYN, SYN_REPORT, 0);
  
  //debug
  printf("Moved pointer by x:%d, y:%d\n", x, y);
}

void initMPointer(char *devName, struct libevdev *dev)
{
  libevdev_set_name(dev, devName);
  
  libevdev_enable_event_type(dev, EV_REL);
  libevdev_enable_event_code(dev, EV_REL, REL_X, NULL);
  libevdev_enable_event_code(dev, EV_REL, REL_Y, NULL);
  
  libevdev_enable_event_type(dev, EV_KEY);
  libevdev_enable_event_code(dev, EV_KEY, BTN_LEFT, NULL);
  libevdev_enable_event_code(dev, EV_KEY, BTN_RIGHT, NULL);
}

int main(void)
{
	struct libevdev *dev;//system device to copy from
	struct libevdev_uinput *magicPointer;//virtual device
	char **result;
	char *line;
	int x = 0;
	int y = 0;
	int i = 0;
	
	int mp = open("/dev/uinput", O_RDWR);

	// Create device
	dev = libevdev_new();
	initMPointer("magicPointer", dev);
	int rc = libevdev_uinput_create_from_device(dev, mp, &magicPointer);
	if (rc < 0)
	{
		printf("Failed to create magicpointer, are you root ?\n");
		return 0;
	}
	libevdev_free(dev); //we don't need the system device
	
	// Allocate line and split it to get the arguments
	while (42)
	  {
	    get_next_line(0, &line);
	    result = ft_strsplit(line, ' ');
	
	    if (result[0] != NULL && result[1] != NULL)
	      {
		x = atoi(result[0]);
		y = atoi(result[1]);
	      }
	    moveMouse(magicPointer, x, y);
	  }
	libevdev_uinput_destroy(magicPointer);

	close(mp);
	// Free alloccated memory
	free(line);
	i = 0;
	while(result[i])
	    free(result[i++]);
	return 0;
}
